package earthquake.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Properties;
import java.util.ArrayList;
import java.time.Duration;
import java.io.IOException;
import java.net.URI;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.opensearch.action.bulk.BulkRequest;
import org.opensearch.action.bulk.BulkResponse;
import org.opensearch.action.index.IndexRequest;
import org.opensearch.action.index.IndexResponse;
import org.opensearch.client.RequestOptions;
import org.opensearch.client.RestClient;
import org.opensearch.client.RestHighLevelClient;
import org.opensearch.client.indices.CreateIndexRequest;
import org.opensearch.client.indices.GetIndexRequest;
import org.opensearch.common.xcontent.XContentType;
import org.json.*;

public class Consumer implements Runnable{

	private static final Logger log = LoggerFactory.getLogger(Consumer.class.getSimpleName());
	private final KafkaConsumer<String, String> consumer;
	final Thread mainThread;
	private final RestHighLevelClient openSearchClient = createOpenSearchClient();


	public static RestHighLevelClient createOpenSearchClient() {
		//String connString = "http://localhost:9200";
		String connString = "secret";

		RestHighLevelClient restHighLevelClient;
		URI connUri = URI.create(connString);
		String userInfo = connUri.getUserInfo();

		if (userInfo == null) {
			// REST client without security
			restHighLevelClient = new RestHighLevelClient(RestClient.builder(new HttpHost(connUri.getHost(), connUri.getPort(), "http")));

		} else {
			// REST client with security
			String[] auth = userInfo.split(":");

			CredentialsProvider cp = new BasicCredentialsProvider();
			cp.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(auth[0], auth[1]));

			restHighLevelClient = new RestHighLevelClient(
					RestClient.builder(new HttpHost(connUri.getHost(), connUri.getPort(), connUri.getScheme()))
					.setHttpClientConfigCallback(
						httpAsyncClientBuilder -> httpAsyncClientBuilder.setDefaultCredentialsProvider(cp)
						.setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy())));


		}

		return restHighLevelClient;
	}

	public Consumer(Properties properties) throws IOException{

		log.info("openSearchClient starting...");
		if(!openSearchClient.indices().exists(new GetIndexRequest("earthquake"), RequestOptions.DEFAULT)){
			CreateIndexRequest createIndexRequest = new CreateIndexRequest("earthquake");
			openSearchClient.indices().create(createIndexRequest, RequestOptions.DEFAULT);
			log.info("earthquake indice created...");
		}else{
			log.info("earthquake indice already exists...");
		}
		log.info("Consumer starting...");
		consumer = new KafkaConsumer<>(properties);
		mainThread = Thread.currentThread();

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				log.info("Shutdown detected");
				consumer.wakeup();
				try {
					mainThread.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void run(){
		log.info("Consumer Running...");
		try {
			String[] topic = {"earthquake"};
			subscripeToTopics(topic);
			BulkRequest bulkRequest = new BulkRequest();
			while (true) {
				log.info("polling consumer");
				ArrayList<String> records = poll();
				for (String record : records) {
					JSONObject jsonObject = new JSONObject(record).getJSONObject("properties");
					String id = jsonObject.getString("code");
					 IndexRequest indexRequest = new IndexRequest("earthquake")
					 	.source(record, XContentType.JSON)
					 	.id(id);

					bulkRequest.add(indexRequest);
				}
				if (bulkRequest.numberOfActions() > 0){
					BulkResponse bulkResponse = openSearchClient.bulk(bulkRequest, RequestOptions.DEFAULT);
					log.info("Inserted " + bulkResponse.getItems().length + " record(s).");
				}
				Thread.sleep(1000);
			}
			
		} catch (Exception e) {
			log.error("error trying to sleep...", e);
		}
	}

	public void subscripeToTopics(String[] topics){
		for (String topic : topics) {
			log.info("Subscriped to topic: " + topic + "\n");
		}
		try {
			this.consumer.subscribe(Arrays.asList(topics));
		} catch (Exception e) {
			log.error("Unexpected error", e);
		}
	}

	public ArrayList<String> poll(){
		ArrayList<String> recordsArray = new ArrayList<String>();
		try {
			//Fetch data, block thread for 1sec
			ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
			for (ConsumerRecord<String, String> record : records) {
				recordsArray.add(record.value());
			}
			return recordsArray;
		} catch (Exception e) {
			log.error("Error while polling.", e);
			return recordsArray;
		}
	}
}
