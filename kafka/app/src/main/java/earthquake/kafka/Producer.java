package earthquake.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.common.errors.*;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Properties;
import java.util.ArrayList;

/**
 * Producer
 */
public class Producer {
	
	private static final Logger log = LoggerFactory.getLogger(Producer.class.getSimpleName());
	private final KafkaProducer<String, String> producer;

	public Producer(Properties properties){
		
		//create producer with properties
		this.producer = new KafkaProducer<String, String>(properties);
	}


	//send single message without partition
	public boolean produce(String topic, String key, String message){
		// log.info("Sending message: \n topic: " + topic + "\n key: " + key + "\n message: " + message + ";\n");
		try {
			this.producer.beginTransaction();
			this.producer.send(new ProducerRecord<String, String>(topic, key, message));
			this.producer.commitTransaction();
		} catch (ProducerFencedException | OutOfOrderSequenceException | AuthorizationException e) {
			log.error("Fatal error, shutting down producer");
			this.producer.close();	
			return false;
		} catch (KafkaException e) {
			log.error("Error, aborting message", e);
			this.producer.abortTransaction();
			return false;
		}
		return true;
	}

	//send single message with partition specified
	public boolean produce(String topic, Integer partition, String key, String message){
		// log.info("Sending message:\n partition: " + partition + " \n topic: " + topic + "\n key: " + key + "\n message: " + message + ";\n");
		try {
			this.producer.beginTransaction();
			this.producer.send(new ProducerRecord<String, String>(topic, partition, key, message));
			this.producer.commitTransaction();
		} catch (ProducerFencedException | OutOfOrderSequenceException | AuthorizationException e) {
			log.error("Fatal error, shutting down producer");
			this.producer.close();	
			return false;
		} catch (KafkaException e) {
			log.error("Error, aborting message", e);
			this.producer.abortTransaction();
			return false;
		}
		return true;
	}


	//send send multiple messages without partition specified, single transaction
	public boolean produce(String topic, ArrayList<String[]> messages){
		log.info("Sending array of messages...");
		try {
			// this.producer.beginTransaction();
			for (String[] content : messages) {
				String key = content[0];
				String message = content[1];
				// log.info("Sending message:\n topic: " + topic + "\n key: " + key + "\n message: " + message + ";\n");
				this.producer.send(new ProducerRecord<String, String>(topic, key, message));
			}
			// this.producer.commitTransaction();
		} catch (ProducerFencedException | OutOfOrderSequenceException | AuthorizationException e) {
			log.error("Fatal error, shutting down producer");
			this.producer.close();	
			return false;
		} catch (KafkaException e) {
			log.error("Error, aborting message", e);
			this.producer.close();	
			// this.producer.abortTransaction();
			return false;
		}
		log.info("Transaction Completed");
		return true;
	}

	//send send multiple messages with partition specified, single transaction
	public boolean produce(String topic, Integer partition, ArrayList<String[]> messages){
		log.info("Sending array of messages to partition:\n" + partition );
		try {
			this.producer.beginTransaction();
			for (String[] content : messages) {
				String key = content[0];
				String message = content[1];
				// log.info("Sending message:\n topic: " + topic + "\n key: " + key + "\n message: " + message + ";\n");
				this.producer.send(new ProducerRecord<String, String>(topic, key, message));
			}
			this.producer.commitTransaction();
		} catch (ProducerFencedException | OutOfOrderSequenceException | AuthorizationException e) {
			log.error("Fatal error, shutting down producer");
			this.producer.close();	
			return false;
		} catch (KafkaException e) {
			log.error("Error, aborting message", e);
			this.producer.abortTransaction();
			return false;
		}
		log.info("Transaction Completed");
		return true;
	}

	public void close(){
		log.info("Closing producer");
		try {
			this.producer.close();
		} catch (Exception e) {
			log.error("Error closing producer", e);
		}
	}

}
