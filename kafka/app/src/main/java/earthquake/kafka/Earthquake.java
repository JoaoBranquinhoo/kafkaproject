package earthquake.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.http.*;
import java.net.http.HttpResponse.BodyHandlers;
import java.net.URI;
import java.util.ArrayList;
import java.util.Properties;
import java.time.*;
import java.time.format.DateTimeFormatter;
import org.json.*;

/**
 * Earthquake
 */
public class Earthquake implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(App.class.getSimpleName());
	private Properties properties;
	private String url;
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS").withZone(ZoneId.systemDefault());
	private Producer producer;

	public Earthquake(Producer producer){
		this.producer = producer;
		log.info("Starting Earthquake API with default settings");

		this.properties = new Properties();
		this.properties.setProperty("api", "https://earthquake.usgs.gov/fdsnws/event/1/");
		this.properties.setProperty("endpoint", "query?");
		this.properties.setProperty("format", "geojson");
		this.properties.setProperty("starttime", formatter.format(Instant.now().plus(Period.ofDays(-1))));
		this.properties.setProperty("endtime", "2025-03-11");
		setUrl();
	}

	public Earthquake(Producer producer, Properties props){
		this.producer = producer;
		log.info("Starting Earthquake with Properties");
		this.properties = props;
		setUrl();
	}

	public void run(){
		try {
			while (true) {
				log.info("polling for earthquakes");
				producer.produce("earthquake", poll());
				Thread.sleep(10000);
			}

		} catch (Exception e) {
			log.error("error trying to sleep...", e);
		}
	}

	public void setUrl() {
		this.url = this.properties.get("api") +
			"" + this.properties.get("endpoint") +  
			"format=" + this.properties.get("format") + 
			"&starttime=" + this.properties.get("starttime") +
			"&endtime=" + this.properties.get("endtime") ;
		log.info("api url= " + url);
	}

	public String getUrl() {
		return url;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public Properties getProperties() {
		return properties;
	}

	public ArrayList<String[]> poll() {
		log.info("Polling for earthquakes... api: " + this.url);

		HttpClient client = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder()
			.uri(URI.create(url))
			.build();
		String response = client.sendAsync(request, BodyHandlers.ofString())
			.thenApply(HttpResponse::body)
			.join(); 

		ArrayList<String[]> dataReady = new ArrayList<String[]>();

		try {
			JSONArray jsonArray = new JSONObject(response).getJSONArray("features");
			for (int i = 0; i< jsonArray.length(); i++) {
				String[] message = {"earthquake", jsonArray.getJSONObject(i).toString()};
				dataReady.add(message);
			}

		} catch (Exception e) {
			log.error("error parsing json", e);
		}

		this.properties.setProperty("starttime", formatter.format(Instant.now()).toString());
		setUrl();
		
		return dataReady;
	}
}
