package earthquake.kafka;

import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

public class App {

	private static final Logger log = LoggerFactory.getLogger(App.class.getSimpleName());

	public static void main(String args[]) throws IOException {
		log.info("Hello, starting...");
	
		//create producer
		Properties producerProp = new Properties();
		producerProp.setProperty("bootstrap.servers", "127.0.0.1:9092");
		producerProp.setProperty("key.serializer", StringSerializer.class.getName());
		producerProp.setProperty("value.serializer", StringSerializer.class.getName());
		Producer producer = new Producer(producerProp);

		//spawn consumer in new thread
		Properties consumerProp = new Properties();
		consumerProp.setProperty("bootstrap.servers", "127.0.0.1:9092");
		consumerProp.setProperty("key.deserializer", StringDeserializer.class.getName());
		consumerProp.setProperty("value.deserializer", StringDeserializer.class.getName());
		consumerProp.setProperty("group.id", "console-consumer-myapp");
		Consumer consumer = new Consumer(consumerProp);

		//EarthQuake api create thread
		Earthquake quake = new Earthquake(producer);

		Thread producerThread = new Thread(quake, "Producer Thread");
		Thread consumerThread = new Thread(consumer, "Consumer Thread");

		producerThread.start();
		consumerThread.start();

		Thread mainThread = Thread.currentThread();

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				log.info("Shutdown detected, kill all....");
				try {
					producerThread.interrupt();
					consumerThread.interrupt();
					mainThread.join();
				} catch (Exception e) {
					log.error("error shutting down threads", e);
				}
			}
		});

		try {
			producerThread.join();
			consumerThread.join();

		} catch (Exception e) {
			log.error("error while waiting for child threads");
		}
	}

}
